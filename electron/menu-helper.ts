import { BrowserWindow, MenuItemConstructorOptions, Menu, app } from 'electron';

export class MenuHelper {
  constructor(private browserWindow: BrowserWindow) {
  }

  private mainMenuTemplate: MenuItemConstructorOptions[] = [
    {
      label: 'File',
      submenu: [
        {
          label: 'Exit',
          role: 'close'
        }
      ]
    },
    {
      label: 'View',
      submenu: [
        {role: 'reload'},
        {role: 'forcereload'},
        {role: 'toggledevtools'},
        {type: 'separator'},
        {role: 'resetzoom'},
        {role: 'zoomin'},
        {role: 'zoomout'},
        {type: 'separator'},
        {role: 'togglefullscreen'}
      ]
    },
    {
      label: 'Help',
      submenu: [
        {
          label: 'About',
          click: () => {
            this.browserWindow.webContents.send('about');
          }
        }
      ]
    }
  ];

  private trayMenuTemplate: MenuItemConstructorOptions[] = [
    {
      label: 'New screenshot',
      click: () => {
        this.browserWindow.webContents.send('screenshot');
      }
    },
    {
      label: 'Exit',
      click: () => { app.quit(); }
    }
  ];

  get mainMenu(): Menu {
    return Menu.buildFromTemplate(this.mainMenuTemplate);
  }

  get trayMenu() {
    return Menu.buildFromTemplate(this.trayMenuTemplate);
  }

}
