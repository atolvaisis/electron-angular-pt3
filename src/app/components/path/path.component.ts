import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { FileInfo } from '../../models/file-info';

@Component({
  selector: 'app-path',
  templateUrl: './path.component.html',
  styleUrls: ['./path.component.scss']
})
export class PathComponent implements OnInit, OnChanges {

  @Input()
  currentPathInfo: FileInfo;

  currentPath: string;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.currentPathInfo.currentValue == null || changes.currentPathInfo.currentValue === '') {
      this.currentPath = '';
    } else {
      this.currentPath = changes.currentPathInfo.currentValue.fullPath;
    }
  }

}
