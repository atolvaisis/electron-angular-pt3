import { Component, OnInit, Input, OnChanges, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { FileService } from '../../services/file.service';
import { FileInfo } from '../../models/file-info';
import { RequiredModulesService } from '../../services/required-modules.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit, OnChanges {

  @Input()
  currentPath: string;

  supportedExtensions: string[] = ['.png', '.jpg', '.jpeg', '.gif', '.bmp', '.svg'];

  imageFiles: FileInfo[];

  constructor(private fileService: FileService,
    private requiredModulesService: RequiredModulesService,
    private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.currentPath.currentValue == null || changes.currentPath.currentValue === '') {
      this.imageFiles = [];
    } else {
      const currentDirectory = changes.currentPath.currentValue;
      this.fileService.getFiles(currentDirectory, this.supportedExtensions).subscribe(f => {
        this.imageFiles = f;
        this.changeDetector.detectChanges();
      });
    }
  }

  openFile(fileInfo: FileInfo) {
    this.fileService.openFile(fileInfo.fullPath);
  }

  deleteFile(fileInfo: FileInfo) {
    const dialog = this.requiredModulesService.electron.remote.dialog;
    const currentWindow = this.requiredModulesService.electron.remote.getCurrentWindow();
    dialog.showMessageBox(currentWindow, {
      type: 'question',
      buttons: ['Yes', 'No'],
      title: 'Confirm',
      message: `Are you sure you want to delete ${fileInfo.name}?`
    }, (choice) => {
      if (choice === 0) {
        this.fileService.deleteFile(fileInfo.fullPath);
        const index = this.imageFiles.indexOf(fileInfo);
        if (index !== -1) {
          this.imageFiles.splice(index, 1);
          this.changeDetector.detectChanges();
        }
      }
    }
    );
  }

}
