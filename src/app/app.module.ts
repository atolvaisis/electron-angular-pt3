import {MatListModule, MatIconModule, MatCardModule, MatDialogModule, MatButtonModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { RequiredModulesService } from './services/required-modules.service';
import { FileService } from './services/file.service';

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/bindNodeCallback';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/empty';
import { DirectoryExplorerComponent } from './components/directory-explorer/directory-explorer.component';
import { PathComponent } from './components/path/path.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { AboutComponent } from './components/about/about.component';
import { ElectronMenuService } from './services/electron-menu.service';
import { ScreenshotService } from './services/screenshot.service';


@NgModule({
  declarations: [
    AppComponent,
    DirectoryExplorerComponent,
    PathComponent,
    GalleryComponent,
    AboutComponent
  ],
  entryComponents: [
    AboutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule
  ],
  providers: [
    RequiredModulesService,
    FileService,
    ElectronMenuService,
    ScreenshotService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
