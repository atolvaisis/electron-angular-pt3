import { Injectable, NgZone } from '@angular/core';
import { RequiredModulesService } from './required-modules.service';
import { MatDialog } from '@angular/material';
import { AboutComponent } from '../components/about/about.component';
import { ScreenshotService } from './screenshot.service';

@Injectable()
export class ElectronMenuService {

  constructor(private requiredModulesService: RequiredModulesService,
    private screenshotService: ScreenshotService,
    private dialog: MatDialog, private ngZone: NgZone) {
      requiredModulesService.electron.ipcRenderer.on('about', () => {
        this.showAboutDialog();
      });

      requiredModulesService.electron.ipcRenderer.on('screenshot', () => {
        this.screenshotService.takeScreenshot();
      });
  }

  showAboutDialog() {
    this.ngZone.run(() => {
      this.dialog.closeAll();
      this.dialog.open(AboutComponent);
    });
  }

}
