import { Injectable } from '@angular/core';
import { RequiredModulesService } from './required-modules.service';

@Injectable()
export class ScreenshotService {

  private screen = this.requiredModulesService.electron.screen;
  private desktopCapturer = this.requiredModulesService.electron.desktopCapturer;
  private remote = this.requiredModulesService.electron.remote;
  private shell = this.requiredModulesService.electron.shell;


  constructor(private requiredModulesService: RequiredModulesService) { }

  private determineScreenshotSize() {
    const screenSize = this.screen.getPrimaryDisplay().workAreaSize;
    const maxDimension = Math.max(screenSize.width, screenSize.height);
    return {
      width: maxDimension * window.devicePixelRatio,
      height: maxDimension * window.devicePixelRatio
    };
  }

  takeScreenshot() {
    const screenshotSize = this.determineScreenshotSize();
    const options = { types: ['screen'], thumbnailSize: screenshotSize };
    this.desktopCapturer.getSources(options, (error, sources) => {
      if (error) {
        console.error(error);
        this.remote.dialog.showErrorBox('Error', error);
      }

      sources.filter(source => source.name === 'Entire screen' || source.name === 'Screen 1').forEach(source => {
        this.remote.dialog.showSaveDialog({filters: [{name: 'PNG Files', extensions: ['png']}]}, (fileName) => {
          if (fileName == null) { return; }
          this.requiredModulesService.fs.writeFile(fileName, source.thumbnail.toPng(), writeError => {
            if (writeError) {
              console.error(writeError);
              this.remote.dialog.showErrorBox('Error', writeError);
            } else {
              this.shell.openExternal(`file://${fileName}`);
            }
          });
        });
      });
    });
  }

}
