import { Component, ChangeDetectorRef } from '@angular/core';
import { FileInfo } from './models/file-info';
import { ElectronMenuService } from './services/electron-menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  currentPathInfo: FileInfo;

  constructor(private changeDetector: ChangeDetectorRef, electronMenuService: ElectronMenuService) {}

  currentPathChanged(fileInfo: FileInfo) {
    this.currentPathInfo = fileInfo;
    this.changeDetector.detectChanges();
  }

}
